
/* scripts entry point */
$(document).ready( function() {

    $("#run3Sum").click( function() {
        $('#resultOf3Sum').text('');
        //let nums = [-1,0,1,2,-1,-4];
        let nums = [-2,0,1,1,2];
        //let nums = [-13,5,13,12,-2,-11,-1,12,-3,0,-3,-7,-7,-5,-3,-15,-2,14,14,13,6,-11,-11,5,-15,-14,5,-5,-2,0,3,-8,-10,-7,11,-5,-10,-5,-7,-6,2,5,3,2,7,7,3,-10,-2,2,-12,-11,-1,14,10,-9,-15,-8,-7,-9,7,3,-2,5,11,-13,-15,8,-3,-7,-12,7,5,-2,-6,-3,-10,4,2,-5,14,-3,-1,-10,-3,-14,-4,-3,-7,-4,3,8,14,9,-2,10,11,-10,-4,-15,-9,-1,-1,3,4,1,8,1];
        $('#resultOf3Sum').text(threeSum(nums));
    });

    $("#runJump").click( function() {
        $('#resultOfJumpgame').text('');
        //let nums = [2,3,1,1,4];
        let nums = [8,2,4,4,4,9,5,2,5,8,8,0,8,6,9,1,1,6,3,5,1,2,6,6,0,4,8,6,0,3,2,8,7,6,5,1,7,0,3,4,8,3,5,9,0,4,0,1,0,5,9,2,0,7,0,2,1,0,8,2,5,1,2,3,9,7,4,7,0,0,1,8,5,6,7,5,1,9,9,3,5,0,7,5];
        $('#resultOfJumpgame').text(jump(nums));
    });
                        
    $("#runBin").click( function() {
        $('#resultOfBinarySub').text('');
        let S = "0110";
        let N = 3;
        $('#resultOfBinarySub').text(queryString(S,N));
    });
                        
    $("#runCountSay").click( function() {
        $('#resultOfCountSay').text('');
        let n = "4";
        $('#resultOfCountSay').text(countAndSay(n));
    });
                        
});

/** 2019.03.27
 * @param {number[]} nums
 * @return {number[][]}
 */
var threeSum = function(nums) {
    let result = [];
    let dups = [];
    nums = nums.sort( (a,b) => a-b );

    for(let i=0; i<nums.length-2; i++) {
        if( nums[i]>0 ) break;  // the rest cannot be less; this is the end
        if( i>0 && nums[i] == nums[i-1] ) continue; // stepped to the next, but it is the same, so gogo
        for(let j=i+1, k=nums.length-1; j<k; ) {    // top-bottom approach
            let sum = nums[i]+nums[j]+nums[k];
            if( sum == 0 ) {    // hit
                result.push([nums[i],nums[j],nums[k]]);
                j++; while( j<k && nums[j]==nums[j-1] ) j++;    // avoid equals
                k--; while( j<k && nums[k]==nums[k+1] ) k--;    // avoid equals
            } else if( sum < 0 )
                j++;
            else
                k--;
        }
    }
    return result;
};

/** 2019.03.25
 * @param {number[]} nums
 * @return {number}
 */
var jump = function(nums) {
    if(nums.length == 1) return 0;      // just one number - we are done
    let shortest = Infinity;
    let memSteps = nums.map( elem => Infinity );
    
    function jumper(step, pos) {
        if( pos+nums[pos] >= nums.length-1 ) {  // can reach the end
            shortest = shortest > step+1 ? step+1 : shortest;   // better solution
        } else if( shortest>step+1 && memSteps[pos]>step ) {              // search more
            memSteps[pos] = step;
            for(let i=nums[pos]; i>0; i-- )
                jumper(step+1, pos+i);
        }
        else {}     // this route is definitely cannot be shorter
    }

    jumper(0,0);
    return shortest;
};

/**
 * @param {string} S
 * @param {number} N
 * @return {boolean}
 */
var queryString = function(S, N) {
    while( N ) {
        if( !S.includes( (N >>> 0).toString(2) ))
            return false;
        --N;
    }
    return true;
};

/** 2019.03.29
 * @param {string} num1
 * @param {string} num2
 * @return {string}
 */
var multiply = function(num1, num2) {
    if( num1 === '0' || num2 === '0' )
        return '0';

    var sums = function(sa, sb) {   // bb MUST not be longer
        let aa = [...sa].map(Number).reverse();
        let bb = [...sb].map(Number).reverse();
        let ret = [];
        let carry = 0;
        let i=0;
        for (i = 0; i < bb.length; i++) {
            let t = aa[i] + bb[i] + carry;
            ret[i] = t % 10;
            carry = (t > 9 ? 1 : 0);
        }
        /* handle the undefined values properly, since they don't cast */
        while (i < aa.length) {
            let t = aa[i] + carry;
            ret[i++] = t % 10;
            carry = (t > 9 ? 1 : 0);
        }
        /* possible carry digit at the end */
        if (carry == 1)
            ret[i] = 1;
        return ret.reverse().join('');
    };
    
    let result = '0';
    let less = (+num1) < (+num2) ? num1 : num2;
    let other = less == num1 ? num2 : num1;

    let digits = [...less];

    while( digits.length ) {
        let digit = (+digits.shift());
        let tNum = other;
        switch( digit ) {
            case 1 :
            case 2 :
            case 3 :
            case 4 :
            case 5 :
            case 6 :
            case 7 :
            case 8 :
            case 9 :
                for(let i=1; i<digit; i++)
                    tNum = sums(tNum,other);
                break;
            default :   // '0'
                tNum = '0';
                break;
        }
        result = result == '0' ? tNum : sums(result,tNum);
        if( digits.length )
            result += '0';
    }
    return result.toString();
};

/** elemzesre...
var multiply = function(num1, num2) {
    let m = num1.length, n = num2.length;
    let res = Array(m + n).fill(0);
    for (let i = m - 1; i >= 0; i --) {
        for (let j = n - 1; j >= 0; j--) {
            let temp = +num1.charAt(i) * +num2.charAt(j);
            let posLow = i + j + 1; // 低位
            let posHeight = i + j;  // 进位
            temp += res[posLow];    // 低进位之和
            res[posLow] = temp % 10; // 获取低位值
            res[posHeight] += Math.trunc(temp / 10) // 得到高位的值
        }
    }

    while (res[0] === 0) {
        res.shift()
    }

    return res.length === 0 ? '0' : res.join('')
};
*/

/** 2019.04.01
 * @param {number} n
 * @return {string}
 */
var countAndSay = function(n) {

    var readOff = function(s) {
        let res = '';
        
        for(let i=0, count=1; i<s.length; i++) {
            if( s[i] != s[i+1] ) {
                res += count.toString() + s[i];
                count = 1;
            } else {
                count++;
            }
        }
        return res;
    };
    
    return n==1 ? '1' : readOff( countAndSay(n-1) );
};
